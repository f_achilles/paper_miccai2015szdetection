\section{Introduction}

Patients suffering from epilepsy are known to exhibit distinct motion patterns during a motor seizure~\cite{Noachtar2009a}.
%Typically, seizures originating from the frontal or lateral lobe lead to clonic or generalized tonic-clonic motions.
%These motions can be described as beginning with a strong tremor in specific limbs and then changing to intermitting convulsions of the whole body when the seizure generalizes.
Every patient presents different motions due to the specific neuroanatomical configuration and pathological behavior of the epileptogenic zone.
The analysis of these motor parts of seizures is an important diagnostic tool, for which a precise separation between ictal phases (during seizures) and interictal phases (inbetween seizures) is required.
%During a strong seizure, patients can sustain significant injuries and in rare cases even die from the effects of the seizure~\cite{nashef1997sudden}.
%It is therefore crucial to detect a seizure onset as early as possible.
%To that purpose, professional staff continuously monitors all patients admitted to the hospital over several days and attends to the patients when required.
To that purpose, patients admitted to an Epilepsy Monitoring Unit (EMU) are under day and night surveillance by trained professionals.
The current gold standard to achieve continuous monitoring and seizure detection in an EMU are Video-EEG systems, which require specialized operators and the attachment of EEG sensors to the patient's scalp.
\begin{figure}[t]
\centering
\includegraphics[width=\textwidth]{TeaserSz5_full_slim.pdf}
\caption{Example of a detected seizure.
The first row show frames from the IR stream, coloured in red.
In frame a), the patient is still asleep.
Seizure starts in frame b), then develops into a strong version and uncontrolled head and leg motion in c) and d); end of the seizure in e).
The second row shows the raw detector output probability for the \textit{seizure} event, values above a defined threshold are highlighted in green.
Ground truth in the third row is green for \textit{true} and blank otherwise.
The detection is successful, but yields spurious false positives after the end of the seizure.}
\label{fig:teaser}
\end{figure}
However, the great majority of motor seizures occurs at home, a significant number of them during night sleep~\cite{provini1999nocturnal}.
Night-time seizures often remain unnoticed as the patient typically does not remember such occurrence after waking up (postictal amnesia).
For the treating neurologist, information about the exact amount of night-time seizures is an important indicator for the effectiveness of the current medication.
%Attaching a scalp EEG or even implanting electrodes, which are the preferred invasive methods for seizure detection in the clinic, are prohibitive in a home environment.
%Other approaches like vibration detectors or wrist-attached accelerometers either provide too little specificity or are hard to maintain~\cite{someone}.
A non-invasive method that is largely maintenance-free and applicable in home monitoring scenarios is automated seizure detection from video~\cite{karayiannis2006automated,cuppens2012,pisani2014,kalitzin2012}.
One important benefit of such automatic detection is that it avoids screening hours of recorded night-time videos.
Most of the time, the patient will be in regular night sleep where no diagnostic information can be retrieved.
A system with a high sensitivity will highlight relevant events in the video for the reviewing physician which can lower the screening time significantly while at the same time keeping the number of missed events very low.

In this work, we present a novel seizure detection method, based on hierarchical features learned with a Convolutional Neural Network (CNN)~\cite{lecun1998gradient}.
%, a \emph{deep} supervised classifier based on feature learning.
The main intuition behind our work is that such a network can learn discriminative features directly from raw input data which can distinguish normal patient's motions from those characteristic of a seizure. This is conversely to the state of the art, which always relies on hand-crafted features (detailed in the next Section). 
%Our main contribution is a network that can learn the specific seizure patterns from raw input data.
%In particular, it captures the temporal correlation in order to distinguish seizure-related motion from normal motion.
For this purpose, we train our CNN in a supervised fashion, by feeding spatio-temporal windows obtained from a combined depth and infrared (IR) data stream (Figure \ref{fig:teaser}).
%\todo{acquired from an off-the-shelf RGB-D sensor (i.e., a Kinect v2).}
The joint use of range and infrared information acquired with an active IR illuminator allows to take into account motion cues extracted from both the geometry and the appearance of the scene being monitored, without the need to illuminate the room at night.
Additionally, it increases the robustness of the system, as daytime and night-time seizures can be captured regardless of the respective lighting conditions.
During testing, each spatio-temporal window is processed in real-time, allowing for fast detection and continuous monitoring.

Conversely to the state of the art, our method does not imply the use of hand-crafted features but learns them via a CNN, this avoiding limitations on the type of motion observed during the seizure: indeed, subtle motions as well as tonic postures and typical clonic convulsions can be detected.
Furthermore, the simultaneous processing of the entire image area rather than just of specific regions or keypoints in the scene yields the inherent capability of handling partial occlusion of the patient, which typically occurs when medical staff enters the room to support the patient during the seizure.
Also, occlusion of the patient body due to bed sheets is handled intrinsically, such that there is no need for a direct view on the patient or even special patient clothing as in~\cite{lu2013quantifying}.
%Also important to note, unlike~\cite{bager2014}, we do not require any marker attached to the patient.
%the detector is trained in a supervised manner from a labelled database.
%As input to the detector we provide a spatio-temporal window which is processed in real-time allowing for fast detection and continuous monitoring.
%Combined depth and infrared streams which are recorded with active IR illumination 

\section{Related Work}
Automatic seizure detection from a video sequence can be described as a binary classification problem where each frame of the sequence needs to be assigned to either the \textit{seizure} or the \textit{non-seizure} class.
Recently, there have been various approaches proposed in literature aimed at this goal.
Karayannis\etal~\cite{karayiannis2006automated} evaluate motion-strength and motion-trajectory features based on optical flow analysis, which are classified with a single-layer neural network.
%For classification, they train a single-layer neural network in 50fold randomized cross-validation.
%Since annotated video data is randomicly subdivided between training and test, the same patient is likely to appear in both sets.
A key difference with respect to our approach is that they provide hand-crafted features to their classifier. 
%A key drawback is the use of a single-layer network as opposed to our deep network, which can learn low level as well as high level hierarchical features and does not require the computation of optical flow or of motion-trajectories.
Pisani\etal~\cite{pisani2014} perform a frequency analysis of the average luminance in order to distinguish between clonic seizure motion and unrelated movements.
%They scan the input stream in an interlaced sliding window approach and obtain real-time performance on 25\,FPS videos using a fixed threshold for classification.
This brings in the limitation that such approach is 
%Although our method shows real-time performance as well, we are not 
dependent on the appearance of specific motion frequencies in the video.
Conversely, our approach is general as it can detect also static and slow unusual patient motions that occur in tonic seizures. % are detected by our system as the CNN is provided with the raw video input regardless of the appearance of specific frequencies.
Another limitation of~\cite{karayiannis2006automated,pisani2014} is that they were proposed to analyze videos from neonate patients only, which are not representative for a general EMU monitoring scenario.
Indeed, detecting seizures within adult or pediatric patients' videos is more complex, as they typically include various motions generated by activities that are not related to seizures, such as leaving the bed, talking to the staff and using everyday objects as laptops, books or their phone.

Cuppens\etal~\cite{cuppens2012} use the Spatio-Temporal Interest Point detector on recordings of pediatric patients in order to find relevant keypoints inside a spatio-temporal window.
Histogram-of-Flow features are computed at these locations % and assigned to 50 cluster centers learned through k-means clustering on a training set.
and classified via SVM. 
%Classification is done via a radial basis function SVM trained in 15fold randomized cross-validation.
%In contrast to~\cite{karayiannis2006automated}, they evaluate same-subject as well as cross-subject performance.
Cuppens\etal~report the dependency on a sufficient amount of detected keypoints, which are not required by CNNs as they perform dense feature extraction from the input.
%Furthermore, the use of hand-crafted features limits the range of application to the specific problem at hand.
%In this case, HoF features capture motion of well-textured areas and will not be able to describe a pathological but static patient pose or the motion of a homogeneous object as the blanket.
%They are only able to capture local motion properties and cannot take global context into account, which in the CNN is achieved through the use of fully connected layers. \\ \indent
Finally, Kalitzin\etal~\cite{kalitzin2012} detect clonic seizures of adult patients and evaluate their method on RGB recordings for daytime seizures and near-infrared recordings for night-time seizures.
They derive robust temporal frequency properties from optical flow and compute the relative spectral component inside a fixed interval of {$2$\,Hz-$6$\,Hz}.
Classification is done via thresholding.
Their algorithm has same limitation as~\cite{pisani2014} as it relies on the appearance of a specific motion frequency during the seizure.

%The state-of-the-art seizure detection approaches either enforce fully controlled environments (neonate recordings~\cite{karayiannis2006automated,pisani2014}) or function only on seizure types with a rhythmical motion component (focal clonic, myoclonic and tonic-clonic seizures~\cite{pisani2014,kalitzin2012}) that produces a defined spectral response in the frequency domain of the input data.
% and do not depend on correct motion-tracking or optical flow computation, as we draw our features from the full spatio-temporal window and not only from specific areas or keypoints in the scene.

%\begin{enumerate}
%\item wristbands (unnecessary)
%\item vibration sensors underneath (unnecessary)
%\item video surveillance
%\end{enumerate}



