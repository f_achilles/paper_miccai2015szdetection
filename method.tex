\section{Method}

% bullet points
%
%\begin{enumerate}
%\item general CNN intro (2 sentences)
%\item "we normalize and stack the input video"
%\item Figure 1: Input $\rightarrow$ Network $\rightarrow$ Output
%\item exact description of network, Learning rate, augmentation
%\item formulas for the general optimization [J(W,b)]
%\item mention possibility of several outputs, put log-softmax-loss equation there
%\end{enumerate}

We rely on a CNN to process a dual input stream of frames acquired from an IR camera and range camera.
In order to learn possible motion features obtained from the comparison of temporally correlated frames, we compute spatio-temporal input volumes from these data streams.
%, the spatial extension being as large as the input frame resolution, while the temporal extension ranging from 1 to n frames. 
The objective is to classify a volume as belonging to either \textit{seizure} or \textit{non-seizure} class.
To create our model, we preprocess the input data (subsection \ref{subsec:Pre}), define the network architecture (subsection \ref{subsec:NetAr}) and finally train the model (subsection \ref{subsec:Train}).

\subsection{Preprocessing}\label{subsec:Pre}
Convolutional operations result in a high output signal, when the input image at the common support region resembles the filter structure.
However, the output is inherently sensitive to peak values in the input, and it will thus always favour bright image regions.
When processing two different modalities, it is therefore necessary to normalize their domains.
The depth sensor provides signals $I_{\mathrm{D}}$ that represent distances limited between $500$\,mm and $4500$\,mm at a resolution of 1\,bit/mm.
In the infrared images $I_{\mathrm{IR}}$ however, values are not limited and extend to the full 16-bit range [0-65525].
We preprocess $I_{\mathrm{IR}}$ with a natural logarithm in order to decrease the high dynamic range.
The streams $I_{\mathrm{D}}$ and $\hat{I}_{\mathrm{IR}} = \log \left(1+I_{\mathrm{IR}}\right) $ are then rescaled to [0-255] while retaining the bit-depth.
In order to discard spurious noise in the depth stream $I_{\mathrm{D}}$ (\eg at occluding edges) we apply a [$3\times3$] median filter kernel and obtain $\hat{I}_{\mathrm{D}}$.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{Network1.pdf}
\caption{Network architecture.
Layers conv(1-3) decrease the feature maps' x- and y-dimension through convolution with $[9\times9]$, $[7\times7]$ and $[5\times5]$ kernels respectively, while increasing the amount of channels from 2\textit{N} to 128.
Fully connected layers fc(4-6) condense the feature maps into $n$ binary outputs.}
\label{fig:architecture}
\end{figure}

%\begin{table}
%\caption{Sizes of filters and feature maps of the network. $N$ is the size of the temporal window, $n$ the number of binary output variables.}
%\begin{tabular}{c|c|c|c|c|c|c}
%layer & conv(1) & conv(2) & conv(3) & fc(4) & fc(5) & fc(6) \\ 
%\hline 
%input map & 100x100x[2$N$] & 46x46x16 & 20x20x64 & 8x8x128 & 1x1x1024 & 1x1x512 \\ 
%output map & 92x92x16 & 40x40x64 & 16x16x128 & 1x1x1024 & 1x1x512 & $n$x1x2 \\ 
%2D conv size & 9x9 & 7x7 & 5x5 & - & - & - \\ 
%\end{tabular} 
%\label{tab:networkParams}
%\end{table}

\subsection{Network Architecture}\label{subsec:NetAr}
As depicted in Figure~\ref{fig:architecture}, the input consists of IR and depth frame pairs $\{ \hat{I}_{\mathrm{IR}}, \hat{I}_{\mathrm{D}} \}$ that are stacked along the temporal dimension from $t_{i-N+1}$ to $t_{i}$, where $i$ represents the index of the current frame and $N$ the number of frames in the spatio-temporal window.
Commonly, CNNs are used to process RGB images where each color defines an input channel~\cite{krizhevsky2012}.
In our architecture however, the channels consist of $N$ IR and depth frame pairs stacked in the time domain, so that instead of three we process $2\cdot N$ input channels.
We compute an average image for IR and depth respectively by summing over all available images in the training set and dividing by the number of images.
This average image pair $\{ \mu_{\mathrm{IR}}^{\mathrm{train}}, \mu_{\mathrm{D}}^{\mathrm{train}} \}$ is subtracted from each input pair, prior to convolution.
The combined spatio-temporal volume is subsequently transformed into a series of feature maps through three computational blocks, each comprised of 1) a convolutional layer, 2) a rectified linear unit (ReLU)~\cite{krizhevsky2012}, 3) a max pooling layer with stride 2 and window size 2 and 4) a local response normalization layer with window size 5, $\alpha=5\cdot10^{-4}$, $\beta=0.75$ and $\kappa=1$ similar to~\cite{krizhevsky2012}.
The feature map provided by the third block is further reduced in size by three fully connected layers, through which the global context can be taken into account.
After the fully connected layers, the last feature map needs to be normalized into $[0,1]$ by a softmax operation in order to represent a probability.
The final output consists of a two-element vector $\left[ q,~1-q \right]$, where $q$ represents the probability for the event to be \textit{true} and reciprocally $1-q$ describes the probability for \textit{false}.
By reconfiguring the fully connected layer fc(6) it is possible to provide $n$ binary outputs in order to detect multiple simultaneous events in the scene.

\subsection{Training}\label{subsec:Train}
In order to learn the connection weights and biases for convolutional and fully connected layers, we append a \textit{logarithmical softmax loss} layer to the network output (Equation \ref{eqn:softmaxloss}):
\begin{equation}
L(x,y) = -\frac{1}{n} \sum_i \log \left(\frac{e^{x_{i,y(i)}}}{\sum_c e^{x_{i,c}}} \right)
\label{eqn:softmaxloss}
\end{equation}
where $x$ is the last feature map of the network, $y$ contains the ground truth class for each output variable and $n$ is the number of output variables.
The loss layer penalizes decisions for the wrong binary class and calculates the derivative with respect to the loss-input $x$.
The derivative is then back-propagated through the network and used to minimize the loss through mini-batch stochastic gradient descent.
%, as shown in Equation~\ref{eqn:LossAndGradientDescent}.
We choose a batch-size of 50 samples and set the learning-rate to $10^{-4}$ for all iterations.
The rest of parameters are set according to~\cite{krizhevsky2012}.
In particular, the momentum is set to $0.9$ and weight decay for L$_2$ regularisation is $5\cdot10^{-4}$.
During training, we apply dropout to the output of fc(4) and fc(5), using a rate of 0.5 similar to~\cite{krizhevsky2012}.
%\begin{align*}
%\label{eqn:LossAndGradientDescent}
%\frac{\partial L(x,y)}{\partial x_i} &= \frac{1}{n} \cdot \left\{
%  \begin{array}{l l}
%    \dfrac{e^{x_{i,\tilde{c}}}}{\sum_c e^{x_{i,c}}} - 1 & \quad \text{if } \tilde{c}=y(i)\\
%    \dfrac{e^{x_{i,\tilde{c}}}}{\sum_c e^{x_{i,c}}} & \quad \text{otherwise}
%  \end{array} \right.
%\\
%\\
%\frac{\partial L}{\partial W_{n}} &= \frac{\partial L}{\partial X_m} \cdot \frac{\partial X_m}{\partial X_{m-1}} \cdots \frac{\partial X_{n+2}}{\partial X_{n+1}} \cdot \frac{\partial X_{n+1}}{\partial W_{n}}
%\\
%\\
%W_{1..n}(\tau+1) &= W_{1..n}(\tau) + \mu\cdot\frac{\partial L}{\partial W_{1..n}(\tau)}
%\end{align*}

\subsection{Data Augmentation}
We augment the training data in order to increase robustness against overfitting and to regularize our model.
To that end, we horizontally flip half of the images in each batch, and randomly shuffle the training set after each epoch to assure variation for the training batches.
A training epoch is defined as the period that the network requires for iterating through all available samples.
%In our training set we cannot guarantee a balance between positive and negative samples, as we use real recordings.\textcolor{red}{ Reformulate: To avoid the loss of generality we do not want to discard a large amount of samples in order to achieve a balanced training set. In contrast, we determine the class $c_{-}\in \mathbf{C}=\{ c_{\mathrm{seizure}}, c_{\mathrm{background}} \}$ which has fewer training samples available and randomly pick the same amount of samples from class $c_+ = \mathbf{C}\setminus c_{-}$ before each epoch.} This way, the network is trained along gradients from positive and negative samples at an equal rate while at the same time using all of the available training data.

